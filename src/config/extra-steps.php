<?php

return [
    "debugger" => [
        'telescope:install',
        'migrate'
    ],
    "role-permission" => [
        'vendor:publish --provider=Spatie\Permission\PermissionServiceProvider',
        'config:clear',
        'migrate'
    ],
    "authentication" => [
        'migrate',
        'passport:install'
    ],
    "queue" => [
        'horizon:install'
    ],
    "search" => [
        'php artisan vendor:publish --provider=Laravel\Scout\ScoutServiceProvider'
    ],
    "payment" => [
        'migrate',
        'vendor:publish --tag=cashier-migrations'
    ],
    "repository-pattern"  => [
        'vendor:publish --provider=Prettus\Repository\Providers\RepositoryServiceProvider'
    ]
];