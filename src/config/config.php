<?php

return [
    "slug"                  => "spatie/laravel-sluggable",
    "role-permission"       => "spatie/laravel-permission",
    "social-integration"    => "laravel/socialite",
    "debugger"              => "laravel/telescope",
    "authentication"        => "laravel/passport",  
    "cache"                 => "predis/predis",
    "queue"                 => "laravel/horizon",
    "search"                => "algolia/scout-extended:^2.0",
    "payment"               => "laravel/cashier",
    "mailer"                => "symfony/mailgun-mailer",
    "image-processor"       => "intervention/image",
    "repository-pattern"    => "prettus/l5-repository",
    "error-monitoring"      => "sentry/sentry-laravel"
];