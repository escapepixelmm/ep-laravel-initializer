<?php

namespace EP\LaravelInitializer\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;

class InstallSpecificPackage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'initializer:install {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install user-defined composer package';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $name = $this->argument('name');

        $predefinedNames = config('initializer');

        if (!array_key_exists($name, $predefinedNames)) {
           $this->error("Please specify the correct package name");
           return;
        }
        $package = $predefinedNames[$name];
        $wantInstall = $this->confirm("{$package} will be installed. Continue?", 'yes');

        if (!$wantInstall) return;
        $path = base_path();
        exec("cd {$path} && composer require {$package}");
        $this->info("Installed Successfully - {$package}");

        Cache::put('specific', $name);
    }
}
