<?php

namespace EP\LaravelInitializer\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;

class ConfigureDefaultPackages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'initializer:configure';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Configure default packages';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $names = Cache::get("default");
        $extraSteps = config('extra-steps');
        $predefinedNames = config('initializer');

        foreach($names as $name) {
            if (!array_key_exists($name, $extraSteps)) {
                continue;
            }
            $package = $predefinedNames[$name];

            $bar = $this->output->createProgressBar(count($names));
            $bar->start();
            $this->newLine();

            $steps = $extraSteps[$name];
            foreach($steps as $step) {
                if (str_contains($step, 'vendor:publish')) {
                    preg_match('^.*=(.*?)$^', $step, $matches);
                    $this->call('vendor:publish', ['--provider'=>$matches[1]]);
                } else {
                    $this->call($step);
                }
            }
            $this->info("Woo hoo! {$package} is ready to use...");
            $bar->advance();
        }
        $bar->finish();
        $names = Cache::forget("default");
    }
}
